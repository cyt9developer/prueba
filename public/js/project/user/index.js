function openDeleteModal(idDelete) {
    $('#idDeleteItem').val(idDelete)
    $('#deleteModal').modal('show');
}


function deleteItem() {
    fetch("/users/" + $('#idDeleteItem').val() + '', {
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'delete'
    })
    .then(function(res) {
      if (res.status == 200) {
        alert("se ha eliminado")
        window.location.href="/users"
      }else{
        alert("problemas al eliminar")
      }  
    })
    .catch(function() {
      alert("Can't connect to backend try latter");
    });
    $('#idDeleteItem').val('');
    $('#deleteModal').modal('hide');
}

$(document).ready(function(){
    $("#search").click(function(){
        window.location.replace("/users" + "?name=" + $("#name").val() + "&last_name=" + $("#last_name").val() + "&gender=" + $("#gender :selected").val() + "&document_type_id=" + $("#document_type_id :selected").val() + "&email=" + $("#email").val())
    });
});