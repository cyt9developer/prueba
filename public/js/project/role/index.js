function openDeleteModal(idDelete) {
    $('#idDeleteItem').val(idDelete)
    $('#deleteModal').modal('show');
}


function deleteItem() {
    fetch("/roles/" + $('#idDeleteItem').val() + '', {
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'delete'
    })
    .then(function(res) {
      if (res.status == 200) {
        alert("se ha eliminado")
        window.location.href="/roles"
      }else{
        alert("problemas al eliminar")
      }  
    })
    .catch(function() {
      alert("Problemas con el servidor");
    });
    $('#idDeleteItem').val('');
    $('#deleteModal').modal('hide');
}

$(document).ready(function(){
    $("#search").click(function(){
        window.location.replace("/roles" + "?name=" + $("#name").val() + "&description=" + $("#description").val())
    });
});