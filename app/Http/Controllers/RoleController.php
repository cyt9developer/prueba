<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('Super usuario');
        $name = $request->name;
        $name = ($name) ? ['name', 'ILIKE', '%'. $name .'%'] : ['id', '!=', 0];
        $description = $request->description;
        $description = ($description) ? ['description', 'ILIKE', '%'. $description .'%'] : ['id', '!=', 0];
        $data = Role::where([
            $name,
            $description
        ]);

        $data->orderBy('created_at', 'DESC');
        $data = $data->paginate(5)->appends(request()->except('page'));
        $data = [
            'data' => $data,
            'name' => $request->name,
            'description' => $request->description
        ];
        return view('roles.index', compact('data'));
    }


    public function create()
    {
        $request->user()->authorizeRoles('Super usuario');
        return view('roles.create', ['role' => new Role()]);
    }

    public function store(RoleRequest $request)
    {
        $request->user()->authorizeRoles('Super usuario');
        $role = new Role();
        $role->fill($request->all());
        $role->save();
        return redirect()->route('roles.index')->with('status', 'Rol creado');
    }

    public function show(Role $role)
    {
        $request->user()->authorizeRoles('Super usuario');
        return view('roles.show', compact('role'));
    }

    public function edit(Role $role)
    {
        $request->user()->authorizeRoles('Super usuario');
        return view('roles.edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {
        $request->user()->authorizeRoles('Super usuario');
        $role->fill($request->all());
        $role->save();
        return redirect()->route('roles.index')->with('status', 'Rol actualizado');
    }

    public function destroy(Role $role)
    {
        $request->user()->authorizeRoles('Super usuario');
        $role->delete();
        return response($role, 200);
    }


}
