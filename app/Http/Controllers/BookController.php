<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['Super usuario', 'Usuario']);
        $data = Book::paginate(10)->appends(request()->except('page'));;
        return view('reports', compact('data'));
    }

}
