<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Role;
use App\Http\Requests\UserRequest;
class UserController extends Controller
{
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('Super usuario');
        $name = $request->name;
        $name = ($name) ? ['name', 'ILIKE', '%'. $name .'%'] : ['id', '!=', 0];
        $last_name = $request->last_name;
        $last_name = ($last_name) ? ['last_name', 'ILIKE', '%'. $last_name .'%'] : ['id', '!=', 0];
        $email = $request->email;
        $email = ($email) ? ['email', 'ILIKE', '%'. $email .'%'] : ['id', '!=', 0];
        $gender = $request->gender;
        $gender = ($gender) ? ['gender',  $gender ] : ['id', '!=', 0];
   
        $data = User::where([
            $name,
            $last_name,
            $email,
            $gender
        ]);

        $data->orderBy('created_at', 'DESC');
        $data = $data->paginate(5)->appends(request()->except('page'));
        $data = [
            'data' => $data,
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'gender' => $request->gender
        ];
        return view('users.index', compact('data'));
    }


    public function create()
    {
        $request->user()->authorizeRoles('Super usuario');
        $roles = Role::select('id', 'name')->get();
        return view('users.create', ['user' => new User(), 'roles' => $roles]);
    }

    public function store(UserRequest $request)
    {
        $request->user()->authorizeRoles('Super usuario');
        $user = new User();
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name); 
            $user->avatar = $name;
        }

        $user->fill($request->except('avatar', 'password', 'role_id'));
        $user->password = bcrypt($request->password);
        $user->save();
        $role = Role::find($user->role_id);
        $user->roles()->attach($role);
        return redirect()->route('users.index')->with('status', 'Usuario creado');
    }

    public function show(User $user)
    {
        $request->user()->authorizeRoles('Super usuario');
        return view('users.show', compact('user'));
    }

    public function edit(User $user)
    {
        $request->user()->authorizeRoles('Super usuario');
        $roles = Role::select('id', 'name')->get();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, User $user)
    {
        $request->user()->authorizeRoles('Super usuario');
        $user->fill($request->except('avatar', 'password', 'role_id'));
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name); 
            $filePathOld = $user->avatar;
            $user->avatar = $name;
        }
        $user->save();
        if($request->hasFile('avatar')){
            $this->deleteImage($filePathOld);
        }
        $role = Role::find($user->role_id);
        if (!$user->hasRole($role))
            $user->attach($role);
        return redirect()->route('users.index')->with('status', 'Usuario actualizado');
    }

    public function destroy(User $user)
    {
        $request->user()->authorizeRoles('Super usuario');
        $filePathOld = $user->avatar;
        $user->delete();
        $this->deleteImage($filePathOld);
        return response($user, 200);
    }

    private function deleteImage($filePathOld){
        if (!empty($filePathOld)) {
            $filePath = public_path().'/images/'.$filePathOld;
            \File::delete($filePath);
        }
    }
}
