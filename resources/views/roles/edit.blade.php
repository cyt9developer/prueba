@extends('layouts.app')
@section('title', 'Editar rol')
@section('content')
<div class="row justify-content-center">
	<div class="col-sm-8">		
		<div class="card">
			<div class="card-header">Editar rol</div>
				<div class="card-body">
					<form method="PUT" action="/roles/{{$role->id}}" class="form-group" >					
					@csrf
					<div class="row">
					@include('roles.fields')
					</div>
						<button type="submit" class="btn btn-primary">Actualizar</button>
						<a href="/users" class="btn btn-danger float-right">Cancelar</a>
					</form>
				</div>
		</div>

	</div>
</div>
@stop
