@extends('layouts.app')
@section('title', 'Listado de roles')
@section('content')
@include('common.success')
<h2>Listado de roles</h2>
<a href="/roles/create" ><span style="font-size:30px" class="fa fa-plus"></span></a>

<p>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter">
    Filtro
  </button>
</p>
	<div class="collapse" id="filter">
	  <div class="card card-body">
	  	<div class="row">
	<div class="form-group  col-sm-4">
		<label>Nombre</label>
		<input type="text" id="name" class="form-control" value="{{$data['name']}}" autofocus >
	</div>
	<div class="form-group   col-sm-4">

	</div>
	<div class="col-sm-4">
		<button id="search" type="submit" class="btn btn-success">Buscar</button>
	</div>

	<div class="form-group col-sm-4">
		<label>Descripcion</label>
		<input type="text" id="description" class="form-control" value="{{$data['description']}}" >
	   
	</div>
	</div>
  </div>
</div>
{{ $data['data']}}
<table class="table table-striped">
	<tr>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Acciones</th>
	</tr>
	@foreach($data['data'] as $role)
	<tr>
		<td>{{$role->name}}</td>
		<td>{{$role->description}}</td>
		<td>
			<a href="/roles/{{$role->id}}" ><span class="fa fa-eye"></span></a>
			<a href="/roles/{{$role->id}}/edit" ><span class="fa fa-edit"></span></a>
			<a href="#" onclick="openDeleteModal({{$role->id}})" ><span class="fa fa-trash"></span></a>
		</td>
	</tr>
	@endforeach
</table>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmacion</h5>
            </div>

            <div class="modal-body">
                ¿Está seguro de eliminar el registro?</br>
                <input type="hidden" id="idDeleteItem" value="" />
                <div class="modal-body">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" onclick="deleteItem()" >Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
<script src="{{ asset('/js/project/role/index.js') }}"></script>
@stop
