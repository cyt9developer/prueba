@extends('layouts.app')
@section('title', 'Crear rol')
@section('content')
@include('common.errors')
<div class="row justify-content-center">
	<div class="col-sm-8">		
		<div class="card">
			<div class="card-header">Crear rol</div>
				<div class="card-body">
					<form method="POST" action="/roles" class="form-group" >
					@csrf
					<div class="row">
					@include('roles.fields')
					</div>
						<button type="submit" class="btn btn-primary">Guardar</button>
						<a href="/users" class="btn btn-danger float-right" >Cancelar</a>
					</form>
				</div>
		</div>

	</div>
</div>

<script type="text/javascript" src="{{asset('/js/project/role/form.js')}}"></script>
@stop

