@extends('layouts.app')
@section('title', 'Ver rol')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<span class="label label-default">Nombre</span>
			{{$role->name}}
			</div>
		</div>	
		<div class="col-md-6">
			<div class="form-group">
				<span class="label label-default">Descripcion</span>
				{{$role->description}}
			</div>
		</div>
	</div>	
</div>
<a href="/roles" class="btn btn-danger float-left">volver</a>

@stop