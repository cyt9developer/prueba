@extends('layouts.app')
@section('title', 'Informe')
@section('content')
@include('common.success')
<h2>Informe de libros</h2>

{{ $data}}
<table class="table table-striped">
	<tr>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Autor</th>
		<th>Fecha publicacion</th>

	</tr>
	@foreach($data as $item)
	<tr>
		<td>{{$item->name}}</td>
		<td>{{$item->description}}</td>
		<td>{{$item->author}}</td>
		<td>{{$item->publication_date}}</td>

	</tr>
	@endforeach
</table>
@stop
