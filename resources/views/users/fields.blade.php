<div class="form-group col-md-6 col-sm-12">
	<label>Nombre</label>
	<input type="text" name="name" class="form-control" value="{{$user->name}}" autofocus required>
</div>
<div class="form-group col-md-6 col-sm-12">
	<label>Apellido</label>
	<input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" autofocus required>
</div>
<div class="form-group  col-md-6 col-sm-12">
  <label>Genero</label>
  <select  name="gender" class="form-control select2" required>
	<option >Seleccione</option>
	<option value="MASCULINO" {{ ( 'MASCULINO' == $user->gender) ? 'selected' : '' }}>Masculino</option>
	<option value="FEMENINO" {{ ( 'FEMENINO' == $user->gender) ? 'selected' : '' }}>Femenino</option>
	<option value="SIN DEFINIR" {{ ( 'SIN DEFINIR' == $user->gender) ? 'selected' : '' }}>Sin definir</option>
  </select>
</div>
<div class="form-group  col-md-6 col-sm-12">
  <label>Roles</label>
  <select name="role_id" class="form-control select2" required>
	<option >Seleccione</option>
	@foreach($roles as $role)
		<option value="{{$role->id}}" {{ ( $role->id == $user->role_id) ? 'selected' : '' }}>{{$role->name}}</option>

	@endforeach
  </select>
</div>
<div class="form-group  col-md-6 col-sm-12">
	<label>Correo</label>
	<input type="email" name="email" class="form-control" value="{{$user->email}}" required>
</div>
<div class="form-group  col-md-6 col-sm-12">
	<label>Contrasena</label>
	<input type="password" name="password" class="form-control" required>
</div>	
<div class="form-group  col-md-6 col-sm-12">
	<label>Imagen</label>
	<input type="file" name="avatar">
</div>