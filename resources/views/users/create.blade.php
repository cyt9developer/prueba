@extends('layouts.app')
@section('title', 'Crear usuario')
@section('content')
@include('common.errors')
<div class="row justify-content-center">
	<div class="col-sm-8">		
		<div class="card">
			<div class="card-header">Crear usuario</div>
				<div class="card-body">
					<form method="POST" action="/users" class="form-group" enctype="multipart/form-data">
					@csrf
					<div class="row">
					@include('users.fields')
					</div>
						<button type="submit" class="btn btn-primary">Guardar</button>
						<a href="/users" class="btn btn-danger float-right" >Cancelar</a>
					</form>
				</div>
		</div>

	</div>
</div>

<script type="text/javascript" src="{{asset('/js/project/user/form.js')}}"></script>
@stop

