@extends('layouts.app')
@section('title', 'Ver usuario')
@section('content')


<div class="container">
<img src="/images/{{$user->avatar}}" style="width: 40px" class="card-img-top">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<span class="label label-default">Nombre</span>
			{{$user->name}}
			</div>
		</div>	
		<div class="col-md-4">
			<div class="form-group">
				<span class="label label-default">Apellido</span>
				{{$user->last_name}}
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<span class="label label-default">Genero</span>
				{{$user->gender}}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<span class="label label-default">Correo</span>
			{{$user->email}}
			</div>
		</div>	

	</div>
	<a href="/users" class="btn btn-danger float-left">volver</a>
</div>


@stop