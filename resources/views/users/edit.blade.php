@extends('layouts.app')
@section('title', 'Editar usuario')
@section('content')
<div class="row justify-content-center">
	<div class="col-sm-8">		
		<div class="card">
			<div class="card-header">Editar usuario</div>
				<div class="card-body">
					<img src="/images/{{$user->avatar}}" style="width: 40px" class="card-img-top">
					<form method="POST" action="/users/{{$user->id}}" class="form-group" enctype="multipart/form-data">
					@method('PUT')						
					@csrf
					<div class="row">
					@include('users.fields2')
					</div>
						<button type="submit" class="btn btn-primary">Actualizar</button>
						<a href="/users" class="btn btn-danger float-right">Cancelar</a>
					</form>
				</div>
		</div>

	</div>
</div>
@stop
