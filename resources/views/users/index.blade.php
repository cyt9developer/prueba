@extends('layouts.app')
@section('title', 'Listado de usuarios')
@section('content')
@include('common.success')
<h2>Listado de usuarios</h2>
<a href="/users/create" ><span class="fa fa-plus"></span></a>

<p>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="filter">
    Filtro
  </button>
</p>
	<div class="collapse" id="filter">
	  <div class="card card-body">
	  	<div class="row">
	<div class="form-group  col-sm-4">
		<label>Nombre</label>
		<input type="text" id="name" class="form-control" value="{{$data['name']}}" autofocus >
	</div>
	<div class="form-group  col-sm-4">
		<label>Apellido</label>
		<input type="text" id="last_name" class="form-control" value="{{$data['last_name']}}" autofocus >
	</div>
	<div class="col-sm-4">
		<button id="search" type="submit" class="btn btn-success">Buscar</button>
	</div>
	<div class="form-group  col-sm-4">
	<label>Genero</label>
	  <select  id="gender" class="form-control select2" required>
		<option value="">Seleccione</option>
		<option value="MASCULINO" {{ ( 'MASCULINO' == $data['gender']) ? 'selected' : '' }}>Masculino</option>
		<option value="FEMENINO" {{ ( 'FEMENINO' == $data['gender']) ? 'selected' : '' }}>Femenino</option>
		<option value="SIN DEFINIR" {{ ( 'SIN DEFINIR' == $data['gender']) ? 'selected' : '' }}>Sin definir</option>
	  </select>
	</div>
	<div class="form-group col-sm-4">
		<label>Correo</label>
		<input type="email" id="email" class="form-control" value="{{$data['email']}}" >
	   
	</div>
	</div>
  </div>
</div>
{{ $data['data']}}
<table class="table table-striped">
	<tr>
		<th>Imagen</th>
		<th>Nombre</th>
		<th>Apellido</th>
		<th>Correo</th>
		<th>Acciones</th>
	</tr>
	@foreach($data['data'] as $user)
	<tr>
		<td><img src="/images/{{$user->avatar}}" style="width: 40px" class="card-img-top"></td>
		<td>{{$user->name}}</td>
		<td>{{$user->last_name}}</td>
		<td>{{$user->email}}</td>
		<td>
			<a href="/users/{{$user->id}}" ><span class="fa fa-eye"></span></a>
			<a href="/users/{{$user->id}}/edit" ><span class="fa fa-edit"></span></a>
			<a href="#" onclick="openDeleteModal({{$user->id}})" ><span class="fa fa-trash"></span></a>
		</td>
	</tr>
	@endforeach
</table>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmacion</h5>
            </div>

            <div class="modal-body">
                ¿Está seguro de eliminar el registro?</br>
                <input type="hidden" id="idDeleteItem" value="" />
                <div class="modal-body">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" onclick="deleteItem()" >Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
<script src="{{ asset('/js/project/user/index.js') }}"></script>
@stop
