@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                <div class="row">
                <div class="col-md-4"><a href="/roles">Roles</a></div>
                <div class="col-md-4"><a href="/users">Usuario</a></div>
                <div class="col-md-4"><a href="/reports">Reporte</a></div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
