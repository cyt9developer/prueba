<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "Super usuario";
        $role->description = "Role del super usuario";
        $role->save();

        $role = new Role();
        $role->name = "Usuario";
        $role->description = "Role del usuario";
        $role->save();
    }
}
