<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\Book;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuper = Role::where('name', 'Super usuario')->first();
        $roleUser = Role::where('name', 'Usuario')->first();

        $user = new User();
        $user->name = "Yolima";
        $user->last_name = "Ramirez";
        $user->avatar = "super.jpeg";
        $user->gender = "Femenino";
        $user->email = "yolima@gmail.com";
        $user->password = bcrypt("123456");
        $user->save();
        $user->roles()->attach($roleSuper);


        $user = new User();
        $user->name = "Sergio";
        $user->last_name = "Cardenas";
        $user->avatar = "usuario.jpeg";
        $user->gender = "Masculino";
        $user->email = "sergio@gmail.com";
        $user->password = bcrypt("123456");
        $user->save();
        $user->roles()->attach($roleUser);


        factory(Book::class, 100)->create();

    }
}
