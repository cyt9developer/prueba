<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->name,
        'author' => $faker->name,
        'publication_date' => $faker->date
    ];
});
